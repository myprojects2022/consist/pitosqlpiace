﻿using System;
using System.Collections.Generic;
using System.IO;
using Models.Data.Class;
using Newtonsoft.Json;
using NLog;

namespace JsonRepository.Data.Class
{
    public static class JsonInstance
    {
        private static readonly Logger logger = LogManager.GetCurrentClassLogger();
        private static RootObjectConfig Root { get; set; }

        private static void Collection(string FileName)
        {
            using (StreamReader reader = new StreamReader(FileName))
            {
                Root = JsonConvert.DeserializeObject<RootObjectConfig>(reader.ReadToEnd());
            };
        }

        public static Dictionary<string, CalcObjectConfig> CollectObjects(string FileName)
        {
            logger.Info("Сборка Json");
            var obj = new Dictionary<string, CalcObjectConfig>();
            try
            {
                Collection(FileName);
                foreach (var item in Root.CalcObjectConfig)
                {
                    obj.Add(item.Target.ToString(), item);
                }
            }
            catch (Exception ex)
            {
                logger.Error($"Не удалось получить коллекцию Json. {ex.Message}");
                throw;
            }
            return obj;
        }

    }
}
