﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FromPItoSQLProcessing.Data.Contracts
{
    public interface IProcessing
    {
        void Initialize();
        void Calculate();
    }
}
