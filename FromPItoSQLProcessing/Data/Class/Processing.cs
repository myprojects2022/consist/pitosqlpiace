﻿using System;
using System.Collections.Generic;
using AfSdkRepository.Data.Class;
using ConfigRepository.Data.Class;
using JsonRepository.Data.Class;
using Models.Data.Class;
using SQLRepository.Data.Class;
using NLog;
using System.IO;
using System.Reflection;
using System.Diagnostics;

namespace FromPItoSQLProcessing.Data.Class
{
    public class Processing 
    {
        private readonly Logger logger = LogManager.GetCurrentClassLogger();
        private readonly CalcObjectFactory CalcObjectFactory;

        private Dictionary<string, string> Config { get; set; }
        private Dictionary<string, CalcObjectConfig> CalcObjectConfigs { get; set; }
        private static Dictionary<string, CalcObject> CalcObjects { get; set; }
        private string AssemblyPath { get; set; }
        private int TimeRange { get; }

        public Processing()
        {
            try
            {
                logger.Info("Сборка");
                CalcObjectFactory = new CalcObjectFactory();
                AssemblyPath = Path.GetDirectoryName(Assembly.GetCallingAssembly().Location);
                Config = ConfigInstance.InstanceSettings(GetType().Assembly.Location);
                TimeRange = int.Parse(Config["TimeRange"]);
                Initialize();
            }
            catch (Exception ex)
            {
                logger.Error($"Сборка не выполнена. {ex.Message}");
                throw;
            }
        }
        private void Initialize()
        {
            try
            {
                GetCalcObjects();
            }
            catch (Exception ex)
            {
                logger.Error($"Ошибка инициализации расчета. {ex.Message}");
                throw;
            }
        }

        private void GetCalcObjects()
        {
            CalcObjectConfigs = JsonInstance.CollectObjects(AssemblyPath + "\\" + Config["JsonFileName"]);
            CalcObjects = CalcObjectFactory.GetCollection(CalcObjectConfigs);
        }

        public void Calculate(DateTime DateTime)
        {
            var sw = Stopwatch.StartNew();
            CheckChanges(DateTime);
            AfsdkInstance.Calculate(CalcObjects, DateTime, TimeRange);
            SQLInstance.Calculate(CalcObjects);
            logger.Info($"Завершен расчет. Выполнено за {sw.Elapsed}");
        }

        private void CheckChanges(DateTime DateTime)
        {
            DateTime lastChanged = File.GetLastWriteTime(AssemblyPath + "\\" + Config["JsonFileName"]);
            TimeSpan timeSpan = DateTime.ToLocalTime() - lastChanged;

            if (timeSpan.TotalSeconds <= TimeRange)
            {
                logger.Info($"Пересоздание коллекции по изменению файла Json. Время изменения файла = {lastChanged}. Разница между временем запуска и изменением = {timeSpan.TotalSeconds} сек.");
                CalcObjects.Clear();
                GetCalcObjects();
            }
        }
    }
}
