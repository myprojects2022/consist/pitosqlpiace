﻿using AfSdkRepository.Data.Class;
using Models.Data.Class;
using NLog;
using System;
using System.Collections.Generic;

namespace FromPItoSQLProcessing.Data.Class
{
    public class CalcObjectFactory
    {
        private readonly Logger logger = LogManager.GetCurrentClassLogger();

        public Dictionary<string, CalcObject> GetCollection(Dictionary<string, CalcObjectConfig> CalcObjectConfigs)
        {
            try
            {
                logger.Info("Запуск сбора коллекции");
                var calcObject = new Dictionary<string, CalcObject>();

                foreach (var item in CalcObjectConfigs)
                {
                    var obj = Create(item.Value);
                    calcObject.Add(item.Key, obj);
                    logger.Info($"Добавлен объект {item.Key}. Режим - {obj.WorkMode}. PI SERVER - {item.Value.PIServerName}. Количество тэгов = {obj.Timeseries.Count}");
                }
                return calcObject;
            }
            catch (Exception ex)
            {
                logger.Error($"Ошибка сбора коллекции. {ex.Message}");
                throw;
            }
        }
        private CalcObject Create(CalcObjectConfig calcObjectConfig)
        {
            var obj = new CalcObject
            {
                Name = calcObjectConfig.Target.ToString(),
                ConnString = ($"Data Source={calcObjectConfig.SQLServer};Initial Catalog={calcObjectConfig.DataBaseName};Persist Security Info=True;Integrated Security=SSPI"),
                Table = calcObjectConfig.TableName,
                WorkMode = (WorkMode)calcObjectConfig.WorkMode
            };
            AfsdkInstance.Server(obj, calcObjectConfig.PIServerName);
            MapTagList(obj, calcObjectConfig.TagArray);
            return obj;
        }
        private void MapTagList(CalcObject obj, List<string> TagList)
        {
            foreach (var item in TagList)
            {
                if (!obj.Timeseries.ContainsKey(item))
                {
                    obj.Timeseries.Add(item, new TimeSeries());
                    AfsdkInstance.Pipoint(obj, item);
                }
            }
        }
    }
}
