﻿using System;
using System.Collections.Generic;
using OSIsoft.AF.PI;
namespace Models.Data.Class
{
    public class CalcObject
    {
        public string Name { get; set; }
        public string ConnString { get; set; }
        public string Table { get; set; }
        public WorkMode WorkMode { get; set; }
        public PIServer Server { get; set; }
        /// <summary>
        ///коллекция тэгов
        /// </summary>
        public Dictionary<string, PIPoint> PointList { get; }
        /// <summary>
        /// колллекция данных по тэгам
        /// </summary>
        public Dictionary<string, TimeSeries> Timeseries { get; }

        public CalcObject()
        {
            Timeseries = new Dictionary<string, TimeSeries>();
            PointList = new Dictionary<string, PIPoint>();
        }

    }
}
