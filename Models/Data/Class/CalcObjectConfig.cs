﻿using System.Collections.Generic;

namespace Models.Data.Class
{
    public class CalcObjectConfig
    {
        public string Target { get; set; }
        public string SQLServer { get; set; }
        public string DataBaseName { get; set; }
        public string TableName { get; set; }
        public string PIServerName { get; set; }
        public byte WorkMode { get; set; }
        public List<string> TagArray { get; }

        public CalcObjectConfig()
        {
            TagArray = new List<string>();
        }
    }
}
