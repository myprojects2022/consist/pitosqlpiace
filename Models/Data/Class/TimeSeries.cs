﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models.Data.Class
{
    public class TimeSeries
    {
        public List<TagValues> TagValue { get; set; }
        public TimeSeries()
        {
            TagValue = new List<TagValues>();
        }
    }
    
}
