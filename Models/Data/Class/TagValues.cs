﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models.Data.Class
{
    public class TagValues
    {
        public double Value { get; set; }
        public DateTime TimeStamp { get; set; }

    }
}
