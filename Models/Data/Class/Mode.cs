﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models.Data.Class
{
    public enum WorkMode : int
    {
        /// <summary>
        /// Рекжим записи всех значений
        /// </summary>
        Full = 0,
        /// <summary>
        /// Режим записи снапшотов на время запуска расчета
        /// </summary>
        Snapshot = 1,
        /// <summary>
        /// Режим перезаписи снапшотов
        /// </summary>
        Rewrite = 2
    }
}
