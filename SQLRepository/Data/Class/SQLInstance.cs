﻿
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Dapper;
using Models.Data.Class;

namespace SQLRepository.Data.Class
{
    public static class SQLInstance
    {
        private static string ConnString { get; set; }
        private static string TableName { get; set; }

        public static void Calculate(Dictionary<string, CalcObject> CalcObjects)
        {
            foreach (var item in CalcObjects)
            {
                Data(item.Value);
            }
        }
        private static void Data(CalcObject calcObject)
        {
            WorkMode workMode = calcObject.WorkMode;
            ConnString = calcObject.ConnString;
            TableName = calcObject.Table;

            foreach (var item in calcObject.Timeseries)
            {
                if (workMode == WorkMode.Rewrite)
                {
                    DatUpdate(item.Value.TagValue, item.Key, workMode);
                }
                else
                {
                    DataInsert(item.Value.TagValue,item.Key, workMode);
                }
                item.Value.TagValue.Clear();
            }
        }

        private static void DataInsert(List<TagValues> tagValues,string TagName, WorkMode workMode)
        {
            using (IDbConnection db = new SqlConnection(ConnString))
            {
                var query = $"INSERT INTO {TableName} (TagName, Value, Timestamp, WriteMode) Values ('{TagName}',@Value, @TimeStamp,'{workMode}')";
                db.Execute(query, tagValues);
            }
        }

        private static void DatUpdate(List<TagValues> tagValues, string TagName, WorkMode workMode)
        {
           using (IDbConnection db = new SqlConnection(ConnString))
            {
                var query = $"exec dbo.MergePIData '{TagName}', @Value, @TimeStamp,'{workMode}'";
                db.Execute(query, tagValues);
            }

        }
    }
}
