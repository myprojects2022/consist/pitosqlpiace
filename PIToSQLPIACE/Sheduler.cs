﻿using System;
using OSIsoft.PI.ACE;
using FromPItoSQLProcessing.Data.Class;

namespace PIACE.PIToSQLPIACE
{
    public class Sheduler : PIACENetClassModule
    {
        private Processing processing;
        public override void ACECalculations()
        {

            DateTime currentTime = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
            currentTime = currentTime.AddSeconds(ExeTime);
            processing.Calculate(currentTime);
        }

        protected override void ModuleDependentInitialization()
        {
            processing = new Processing();
        }
        protected override void InitializePIACEPoints()
        { }

        protected override void ModuleDependentTermination()
        { }
    }
}
