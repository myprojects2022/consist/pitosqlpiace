﻿using System;
using System.Collections.Generic;
using Models.Data.Class;
using NLog;
using OSIsoft.AF.PI;
using OSIsoft.AF.Time;

namespace AfSdkRepository.Data.Class
{
    public static class AfsdkInstance
    {
        private static readonly Logger logger = LogManager.GetCurrentClassLogger();

        public static void Server(CalcObject calcObject, string Piserver)
        {
            try
            {
                PIServers Servers = new PIServers();
                calcObject.Server = Servers[Piserver];
            }
            catch (Exception ex)
            {
                logger.Error($"Не удалось получить PI SERVER {Piserver}. {ex.Message}");
                throw;
            }
        }

        public static void Pipoint(CalcObject calcObject, string Tagname)
        {
            try
            {
                PIPoint pipoint = PIPoint.FindPIPoint(calcObject.Server, Tagname);
                calcObject.PointList.Add(Tagname, pipoint);
            }
            catch (Exception ex)
            {
                logger.Warn($"Не удалось получить Тэг {Tagname} или добавить его в объект расчета.{ex.Message}");
                throw;
            }
        }

        public static void Calculate(Dictionary<string, CalcObject> CalcObjects, DateTime DateTime, int TimeRange)
        {
            try
            {
                AFTimeRange afTimeRange = new AFTimeRange(new AFTime(DateTime.AddSeconds(-TimeRange)), new AFTime(DateTime));
                foreach (var item in CalcObjects)
                {
                    AfSdkData.Data(item.Value, afTimeRange);
                }
            }
            catch (Exception ex)
            {
                logger.Error($"Ошибка при выполнении метода расчета - получения данных из PI. {ex.Message}");
                throw;
            }
        }
    }
}
